<?php  
    include_once('dbFunction.php');  
       $obj=new dbFunction();

        if(isset($_POST['login'])){  
            $emailid = $_POST['emailid'];  
            $password = $_POST['password']; 

            $user = $obj->Login($emailid, $password);  
            if ($user) {  
                // Registration Success  
               header("location:home.php");  
            } else {  
                // Registration Failed  
                echo "<script>alert('Emailid / Password Not Match')</script>";  
            }  
        }  
        if(isset($_POST['submit']))
        {
            $username = $_POST['username'];  
            $emailid = $_POST['emailid'];  
            $password = $_POST['password'];  
            $confirmPassword = $_POST['confirm_password'];  
            if($password == $confirmPassword){  
                $email = $obj->isUserExist($emailid);  
                if(!$email){  
                    $register = $obj->Register($username, $emailid, $password);  
                    if($register){  
                         echo "<script>alert('Registration Successful')</script>";  
                    }else{  
                        echo "<script>alert('Registration Not Successful')</script>";  
                    }  
                } else {  
                    echo "<script>alert('Email Already Exist')</script>";  
                }  
            } else {  
                echo "<script>alert('Password Not Match')</script>";  
              
            }  
      }
?>
<!DOCTYPE html>
 <html lang="en" class="no-js">  
 <head>  
        <meta charset="UTF-8" />  
        <title>Login and Registration Form with HTML5 and CSS3</title>  
        <meta name="viewport" content="width=device-width, initial-scale=1.0">   
        <meta name="description" content="Login and Registration Form with HTML5 and CSS3" />  
        <meta name="keywords" content="html5, css3, form, switch, animation, :target, pseudo-class" />  
        <meta name="author" content="Codrops" />  
        <link rel="shortcut icon" href="../favicon.ico">   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">  
       
    </head>  
    <body>  
        <div class="container">  
              
              
            <header>  
                <h1>Login and Registration Form  </h1>  
            </header>  
            <section>               
                <div id="container_demo" >  
                     
                    <a class="hiddenanchor" id="toregister"></a>  
                    <a class="hiddenanchor" id="tologin"></a>  
                    <div id="wrapper">  
                        <div id="login" class="animate form">  
                           <form name="login" method="post" action="">  
                                <h1>Log in</h1>   
                                <p>   
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>  
                                    <input id="emailsignup" name="emailid" required="required" type="email" placeholder="mysupermail@mail.com"/>   
                                </p>  
                                <p>   
                                    <label for="password" class="youpasswd" data-icon="p"> Your password </label>  
                                    <input id="password" name="password" required="required" type="password" placeholder="eg. X8df!90EO" />   
                                </p>  
                                <p class="keeplogin">   
                                    <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" />   
                                    <label for="loginkeeping">Keep me logged in</label>  
                                </p>  
                                <p class="login button">   
                                    <input type="submit" name="login" value="Login" />   
                                </p>  
                                <p class="change_link">  
                                    Not a member yet ?  
                                    <a href="#toregister" class="to_register">Join us</a>  
                                </p>  
                            </form>  
                        </div>  
  
                        <div id="register" class="animate form">  
                            <form name="submit" method="post" action="">  
                                <h1> Sign up </h1>   
                                <p>   
                                    <label for="usernamesignup" class="uname" data-icon="u">Your username</label>  
                                    <input id="usernamesignup" name="username" required="required" type="text" placeholder="mysuperusername690" />  
                                </p>  
                                <p>   
                                    <label for="emailsignup" class="youmail" data-icon="e" > Your email</label>  
                                    <input id="emailsignup" name="emailid" required="required" type="email" placeholder="mysupermail@mail.com"/>   
                                </p>  
                                <p>   
                                    <label for="passwordsignup" class="youpasswd" data-icon="p">Your password </label>  
                                    <input id="passwordsignup" name="password" required="required" type="password" placeholder="eg. X8df!90EO"/>  
                                </p>  
                                <p>   
                                    <label for="passwordsignup_confirm" class="youpasswd" data-icon="p">Please confirm your password </label>  
                                    <input id="passwordsignup_confirm" name="confirm_password" required="required" type="password" placeholder="eg. X8df!90EO"/>  
                                </p>  
                                <p class="signin button">   
                                    <input type="submit" name="submit" value="submit"/>   
                                </p>  
                                <p class="change_link">    
                                    Already a member ?  
                                    <a href="#tologin" class="to_register"> Go and log in </a>  
                                </p>  
                            </form>  
                        </div>  
                          
                    </div>  
                </div>    
            </section>  
        </div>  
    </body>  
</html> 